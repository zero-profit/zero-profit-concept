# Price
The price of a ZPC company product or service must be equal to the company expenses over a recent small period of time, divided by the amount of sales during the aforementioned period. These expenses include all categories of expenses including the salaries and the difference of the DM and the DA. In case the calculated price exceeds a predefined maximum price (PM) then the final product price is that predefined maximum price (PM). The function that gives the product price is the following:

```math
price_t = \begin{cases}
  \frac{expenses}{sales} \text{ if ${price_t}\le${PM}}\\
  price\_max
  \end{cases}
```

# Period Length
The length of the chosen period is important because the price calculation is a feedback method. In edge cases big period lengths can result in miscalculations as the calculated product price will not cover the company needs anymore. The period length must be long and short enough so to take account more realistic average amounts. The methods for choosing an optimal time length are yet to be discovered.

### Maximum price
The maximum price may be the average price of the same products in the market. The maximum price intends to keep the product in the market in periods the calculated price is much bigger than the other products in the market. If the calculated price was to be used In such periods then the product sales would drop as the buyers would choose other products.

Periods where the calculated price is greater than the maximum price are not always periods of crisis for the company. These periods can be expected when the difference between DM and DA is high, which can occur when the company is at its early days or when it has just passed a period of crisis.
