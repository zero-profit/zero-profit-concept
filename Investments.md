# Investments

## Public Investments
Since ZP companies do not keep profits they provide the lowest prices by definition and as they keep growing and updating they discover more efficient production methods. As a result consumers pay the minimum price for a given product if they buy it from a ZP company. With that in mind, ZP companies could look for initial capital in individuals as in the long term they will pay less if their purchases come from a ZP company.
