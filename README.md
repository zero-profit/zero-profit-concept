# Zero-Profit company model

The zero profit (ZP) company model attempts to define a self-sustainable, conservative, fair-reward company. The main principle of a ZP company is that it keeps its income equal to its expenses by constantly changing its product price. As a result ZP companies are following the opposite of the "law of supply and demand"

ZP companies change their product price periodically based on the last period's sales and expenses. The price is equal to the company expenses, divided by the amount of products sold during the aforementioned period. In case the calculated price exceeds a predefined maximum price then the final product price is that predefined maximum price:

```math
price_t = \begin{cases}
  \frac{expenses}{sales} \text{ if ${price_t}\le${price\_max}}\\
  price\_max
  \end{cases}
```

## Appendix
1. [Expenses](Expenses.md)
2. [Price](Price.md)
3. [Investments](Investments.md)

## Contact
Create an issue or send an email to [zero-profit-concept@protonmail.com](mail:zero-profit-concept@protonmail.com). Pull requests, dicussions etc are more than welcome!
