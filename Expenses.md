# Expenses
As company expenses are considered all the production costs, including the salaries, plus some money for a deposit that is meant for covering financial damages and updating or expanding the company. The production costs are expenses like rent, cost of materials, maintenance costs etc, the salaries of the employees, employers and the difference of the company's money deposit. The costs that are not related with salaries or the deposit are handled in the same way as in the usual company models and will not be discussed further.

## Salaries
In a ZPC company the owners, employers and employees salaries are within a fixed range of salaries. The upper limit of the salary range has to be lower or equal to the lower limit multiplied by a salary factor (sf). This factor should be optimized based on the local criteria. Additionally, during the company life the lower and upper limits can change while keeping its dependencies to each other.

The main idea behind the salary model is that all salaries must allow a high living standard, while allowing for differences based a person's working schedule.

## Deposit
The company money deposit is used for the company's expansion, keeping the company up to date, covering financial damages or anything else that may raise. The actual amount of money in the company deposit (DA) must not exceed a maximum amount (DM). Other than the DM, the company deposit works as in the usual company models. The difference between the DA and the DM is included in the expenses and it is affecting the product price.

The company deposit is a key element of a ZPC company as it is tightly connected to the company's life. In case the sales decrease, the company will have to raise the product price. If the price exceeds the maximum allowed price, the company will have to use the deposit to cover the damage.

The DM must be always defined but it can be changed through the company's life. Raising the DM can happen freely. Reducing the DM can not because it could lead to a positive difference between the DA and the new DM. Reductions of the DM can only happen when the DA is less than the new DM.

Calculating a realistic DM is not a standard procedure and the methods and the DMs can highly vary among the ZPC companies.

Furthermore, this deposit is intended to refrain the company from taking loans as they would make the company dependent on other organizations or individuals in near-bankruptcy situations.

